# Muscula Java Logback Appender

## Configuration

1. Add dependency to pom file
```xml
<dependency>
    <groupId>com.muscula</groupId>
    <artifactId>muscula-log4j</artifactId>
    <version>1.0.0</version>
</dependency>
```

1. Configure log4j in file src/main/resources/log4j2.xml, remember to enter YOUR_MUSCULA_LOG_ID

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="INFO">
    <Appenders>
        <MusculaAppender name="MusculaAppender" logId="YOUR_MUSCULA_LOG_ID"/>
    </Appenders>
    <Loggers>
        <Root level="error">
            <AppenderRef ref="MusculaAppender"/>
        </Root>
    </Loggers>
</Configuration>
```
