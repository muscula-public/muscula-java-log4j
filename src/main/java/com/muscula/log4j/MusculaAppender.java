package com.muscula.log4j;

import com.muscula.core.Credentials;
import com.muscula.core.Logs;
import com.muscula.core.model.MusculaLog;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.impl.MutableLogEvent;

import java.io.Serializable;

/**
 * Copyright Universe Surf Gmbh
 * Created by Sebastian Chmielinski (chmielinski.sebastian@gmail.com) on 25.08.2020
 */
@Plugin(
        name = "MusculaAppender",
        category = Core.CATEGORY_NAME,
        elementType = MusculaAppender.ELEMENT_TYPE)
public class MusculaAppender extends AbstractAppender {

    protected MusculaAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions, Property[] properties) {
        super(name, filter, layout, ignoreExceptions, properties);
    }

    @PluginFactory
    public static MusculaAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginAttribute("endpointUrl") String endpointUrl,
            @PluginAttribute("logId") String logId,
            @PluginElement("Filter") Filter filter) {

        Credentials.setLogId(logId);
        Credentials.setEndpointUrl(endpointUrl);
        return new MusculaAppender(name, filter, null, false, new Property[] {Property.createProperty("logId", logId)});
    }

    /**
     * Converts log4j2 log event to Muscula error model.
     * Only error logs with exception are passed to muscula
     * @param logEvent
     */
    @Override
    public void append(LogEvent logEvent) {
        Object[] params = null;

        if(logEvent instanceof MutableLogEvent) {
            params = ((MutableLogEvent)logEvent).getParameters();
        }
        Logs.push(new MusculaLog(logEvent.getMessage().getFormattedMessage(), logEvent.getLevel().name(), logEvent.getThrown(), params));
    }
}
